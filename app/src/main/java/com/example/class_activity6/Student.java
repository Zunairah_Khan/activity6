package com.example.class_activity6;

public class Student {
    private String First_name;
    private String Last_name;
    private String age;
    private String enrollement_no;
    private String class_c;

    public String getFirst_name() {
        return First_name;
    }
    public void setFirst_name(String first_name) {
        First_name = first_name;
    }

    public String getLast_name() {
        return Last_name;
    }
    public void setLast_name(String last_name) {
        Last_name = last_name;
    }

    public String getAge() {
        return age;
    }
    public void setAge(String age) {
        this.age = age;
    }

    public String getEnrollement_no() {
        return enrollement_no;
    }
    public void setEnrollement_no(String enrollement_no) {
        this.enrollement_no = enrollement_no;
    }

    public String getClass_c() {
        return class_c;
    }
    public void setClass_c(String class_c) {
        this.class_c = class_c;
    }


}
